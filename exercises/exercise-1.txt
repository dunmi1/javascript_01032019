Exercise 1 - Functions & Arrays

https://gitlab.com/t4d-classes/javascript_01032019

1. Create an array of three fruit objects. Each fruit object should have the following properties:

id: number (1, 2, 3, ...)
name: string
weight: number (float)
price: number (float)
quantity: number (int)

2. Define a function which accepts an array of fruit as the first parameter and a fruit object as the second parameter. In the function return back a new array which add the fruit to the end of the array.

Call the function to add two additional fruits to the array beyond the three you start with.

3. Display the number of fruits in the array, the total weight of fruits in the array, the total price of fruit in the array.

4. Sales tax of 4.5%. Multiply the sales tax against the subtotal to calculate the total price to buy the fruit in the array.

Use 'console.log' for all of your output.