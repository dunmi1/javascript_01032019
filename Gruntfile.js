module.exports = function(grunt) {

  grunt.registerTask('task-three', 'Task Three', function() {
    grunt.log.write('Task Three');
  });


  grunt.registerTask('task-two', 'Task Two', function() {

    const done = this.async();

    grunt.log.write('Task Two');

    setTimeout(() => {
      grunt.log.write('Task Two Done');
      done();
    }, 2000);

  });
  
  
  grunt.registerTask('task-one', 'Task One', function(p1, p2) {

    grunt.log.write('Task One ' + [p1, p2].join(' '));

  });

  // grunt.registerTask('default', 'Some desc...', function() {

  //   grunt.task.run('task-one:one:two');

  //   grunt.log.write('Welcome to Grunt!');

  // });


  grunt.registerTask('default', ['task-two']);


};